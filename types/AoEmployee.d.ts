export default interface AoEmployee {
  id: number
  accountObjectId: number
  OrgUnitId: number
  aoGroupId: number
  isCustomer: number
  isVendor: number
  employeeCode: string
  employeeName: string
  gender: number
  birthDate: string
  idNo: string
  idIssueDate: string
  idIssuePlace: string
  jobTitle: string
  receiveAccountId: number
  payAccountId: number
  contactAddress: string
  contactTel: string
  contactMobile: string
  contactEmail: string
  status: number
  createBy: number
  createTime: string
  updateBy: number
  updateTime: string
}
