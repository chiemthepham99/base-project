import { encrypt } from './util'
import config from '@/config/defaultSettings'

interface storageOptType {
  namespace?: string
  storage?: string
  default_cache_time?: number
  isEncrypt?: boolean
}

const options = Object.assign(
  {
    namespace: 'ls_', // key prefix
    storage: 'localStorage', // storage name session, local, memory
    default_cache_time: 60 * 60 * 24 * 365, // One year
    isEncrypt: false
  },
  config.storage
)

let hasSetStorage = false

export const storage = {
  getKey: (key: string): string => {
    return options.namespace + key
  },
  setOptions: (opt: storageOptType): void => {
    if (hasSetStorage) {
      console.error('Has set storage:', options)
      return
    }
    Object.assign(options, opt)
    hasSetStorage = true
  },
  set: (key: string, value: any, expire: number | null = options.default_cache_time) => {
    const stringData = JSON.stringify({
      value,
      expire: expire !== null ? new Date().getTime() + expire * 1000 : null
    })
    window[options.storage].setItem(
      storage.getKey(key),
      options.isEncrypt ? encrypt.encryptByAES(stringData) : stringData
    )
  },
  setObj: (key: string, newVal, expire: number | null = options.default_cache_time): void => {
    const oldVal = storage.get(key)
    let val = newVal
    if (oldVal) {
      if (typeof oldVal === 'object') {
        val = Object.assign(oldVal, newVal)
      }
    }
    storage.set(key, val, expire)
  },
  get: (key: string): any => {
    let item = window[options.storage].getItem(storage.getKey(key))
    if (item) {
      try {
        if (options.isEncrypt) {
          item = encrypt.decryptByAES(item)
        }
        const data = JSON.parse(item)
        const { value, expire } = data
        if (expire === null || expire >= Date.now()) {
          return value
        }
        storage.remove(storage.getKey(key))
      } catch (e) {
        console.error(e)
      }
    }
    return null
  },
  remove: (key: string): void => {
    window[options.storage].removeItem(storage.getKey(key))
  },
  clear: (): void => {
    window[options.storage].clear()
  }
}

export default storage
