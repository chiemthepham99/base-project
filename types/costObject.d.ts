export interface CostObject {
  id: number
  name: string
  code: string
  type: string
  description: string
  listProductItemId: Array<any>
  orgUnitId: any
}
