import { executeProductService } from '@/services'
import API from '@/services/defineRouting'

export async function getProductList(params?: { lineOfBusinessId?: number }) {
  return executeProductService(API.productList, params)
}
