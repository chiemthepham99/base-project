export default function getIconsProductItemType() {
  return Promise.all([
    import('../assets/images/icon1.png'),
    import('../assets/images/icon2.png'),
    import('../assets/images/icon3.png'),
    import('../assets/images/icon4.png'),
    import('../assets/images/icon5.png')
  ])
}
