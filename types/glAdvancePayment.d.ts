import dayjs from 'dayjs'
import { Nullable } from '#/global'

export interface AdvancePayment {
  id: Nullable<number>
  isReceiptExcessMoney: boolean
  accountingMultiInv?: boolean
  listAdvance: Array<ListAdvance>
  agtCategoryCode: number | undefined
  agtCode: string
  agtCodeNumberDigit: number | undefined
  agtCodePrefix: string
  agtCodeSuffix: string
  agtMemo: string
  isRecord?: boolean
  aoEmployeeId: number | undefined
  currencyExcRate: number | undefined
  currencyId: number | undefined
  finalisationEachAdvance: true
  listDetail: Array<ListDetailAdvancePayment>
  listInvTax: Array<ListDetailInvTax>
  listVoucher: Array<any>
  overUnpaidAmount: number | undefined
  overUnpaidAmountOc: number | undefined
  postedDate: Date | string | dayjs.Dayjs
  totalAmount: number | undefined
  totalAmountAdvance: number | undefined
  totalAmountAdvanceOc: number | undefined
  totalAmountOc: number | undefined
  voucherDate: Date | string | dayjs.Dayjs
  voucherId: Nullable<number>
  fromDate: Date | string | dayjs.Dayjs
  toDate: Date | string | dayjs.Dayjs
}

export interface ListAdvance {
  accountTransCode: string
  accountTransId: number | undefined
  amount: number | undefined
  amountFinalisation: number | undefined
  amountFinalisationOc: number | undefined
  amountOc: number | undefined
  creAccountId: number | undefined
  creAccountNumber: string
  debAccountId: number | undefined
  debAccountNumber: string
  description: string
  postedDate: Date | string | dayjs.Dayjs
  voucherDate: Date | string | dayjs.Dayjs
}

export interface ListAdvancePayment {
  accountTransDetailId: number | undefined
  id: number | undefined
  accountTransMemo: string
  accountingVoucherId: number | undefined
  amount: number | undefined
  atCode: string
  creAccountId: number | undefined
  creAccountNumber: string
  debAccountId: number | undefined
  debAccountNumber: string
  postedDate: Date | string | dayjs.Dayjs
  remainingAdvanceAmount: number | undefined
  voucherDate: Date | string | dayjs.Dayjs
  settlementThisTime: number | undefined
}

export interface ListDetailAdvancePayment {
  amount: number | undefined
  amountOc: number | undefined
  constructionId: number | undefined
  costObjectId: number | undefined
  creAccountId: number | undefined
  creAccountNumber: string
  creObjectCode: string
  creObjectId: number | undefined
  creObjectName: string
  isEdit?: boolean
  debAccountId: number | undefined
  debAccountNumber: string
  debObjectCode: string
  debObjectId: number | undefined
  debObjectName: string
  description: string
  orgUnitId: number | undefined
  productionCostItemId: number | undefined
}
export interface ListVoucher {
  code: string
  id: number | undefined
  ignoreCheckExist?: boolean
}
export interface ListDetailInvTax {
  accountObjectCode: string
  accountObjectId: number | undefined
  accountObjectName: string
  accountObjectTaxCode: string
  description: string
  invoiceDate: Date | string | dayjs.Dayjs
  invoiceNumber: string
  isEdit?: boolean
  purchaseProductGroup: number | undefined
  taxType: number | undefined
  valueBeforeTax: number | undefined
  vatAccountId: number | undefined
  vatAccountNumber: string
  vatAmount: number | undefined
  vatPercent: number | undefined
  withInvoice: true
  vatPercentType: number | undefined
}
