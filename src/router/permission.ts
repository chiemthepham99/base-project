import store from '@/store'

export function hasRoutePermission(route) {
  const permissions = store.getters.permissions
  if (route.meta && route.meta.permission && permissions && Array.isArray(permissions)) {
    return (
      route.meta.permission &&
      Array.isArray(route.meta.permission) &&
      route.meta.permission.some((s) => permissions.includes(s))
    )
  }
  return true
}

export function hasRole(permission, route) {
  if (route.meta && route.meta.permission) {
    return route.meta.permission.includes(permission.id)
  } else {
    return true
  }
}

export function filterRouterPermission(routerMap) {
  return routerMap.filter((route) => {
    if (hasRoutePermission(route)) {
      if (route.children && route.children.length) {
        route.children = filterRouterPermission(route.children)
      }
      return true
    }
    return false
  })
}
