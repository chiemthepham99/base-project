import { Nullable } from '#/global'
import Model from '#/Model'

export default interface SalesTransDetail extends Model {
  productItemId?: Nullable<number> // Id hàng hóa
  productItemCode: string // Mã hàng hóa
  productItemName: string // Tên hàng hóa
  costAccountId?: Nullable<number> // Id tài khoản chi phí
  costAccountNumber?: string // Số tài khoản chi phí
  creAccountId: Nullable<number> // Id tài khoản doanh thu
  creAccountNumber: Nullable<string> // Số tài khoản doanh thu
  costUnitPrice: Nullable<number>
  costAmount: Nullable<number>
  currencyExcRate: Nullable<number>
  inventoryId?: Nullable<number> // Số tài khoản chi phí
  inventoryAccountId?: Nullable<number> // Id tài khoản kho
  inventoryAccountNumber?: string // Số tài khoản kho
  debAccountId?: Nullable<number> //  Id tài khoản công nợ
  debAccountNumber?: string // Số tài khoản công nợ
  unitId: Nullable<number> // Đơn vị tính
  quantity: Nullable<number> // Số lượng
  unitPriceOc: Nullable<number> // Số lượng
  unitPrice: Nullable<number> // Số lượng
  amountOc: Nullable<number> // Thành tiền theo đơn vị tiền tệ gốc
  amount: Nullable<number> // Thành tiền sau quy đổi
  vatPercent: Nullable<number> // Phần trăm thuế
  vatPercentType: Nullable<number> // Phần trăm thuế
  vatAmountOc: Nullable<number> // Tiền thuế theo đơn vị tiền tệ gốc
  vatAmount: Nullable<number> // Tiền thuế sau quy đổi
  discountAccountId?: number
  discountAccountNumber?: number
  discountRate: Nullable<number> // Tỷ lệ chiết khấu
  discountAmountOc: Nullable<number> // Tiền chiết khấu theo đơn vị tiền tệ gốc
  discountAmount: Nullable<number> // Tiền chiết khấu sau quy đổi
  vatAccountId?: Nullable<number> // Id Tài khoản thuế
  vatAccountNumber?: Nullable<number> // Số tài khoản thuế
  inventoryAmount: Nullable<number> // Giá trị nhập kho
  isEdit: boolean
}
