import dayjs from 'dayjs'

export interface GeneralTransaction {
  accountingMultiInv?: boolean
  agtCategoryCode: number | undefined
  agtCode: string
  agtCodeNumberDigit: number | undefined
  agtCodePrefix: string
  agtCodeSuffix: string
  agtMemo: string
  currencyExcRate: number | undefined
  currencyId: number | undefined
  overUnpaidAmount: number | undefined
  overUnpaidAmountOc: number | undefined
  paymentDueDate: string
  postedDate: Date | string | dayjs.Dayjs
  totalAmount: number | undefined
  totalAmountAdvance: number | undefined
  totalAmountAdvanceOc: number | undefined
  totalAmountOc: number | undefined
  isRecord?: boolean
  id?: number | null
  voucherDate: Date | string | dayjs.Dayjs
  listDetail: Array<ListDetailGeneralTransaction>
  listInvTax: Array<ListDetailInvTax>
  listVoucher: Array<ListVoucher>
}

export interface ListDetailGeneralTransaction {
  productionCostItemId: number | undefined
  amount: number | undefined
  amountOc: number | undefined
  constructionId: number | undefined
  costObjectId: number | undefined
  creAccountId: number | undefined
  creAccountNumber: string
  creObjectCode: string
  creObjectId: number | undefined
  creObjectName: string
  debAccountId: number | undefined
  debAccountNumber: string
  debObjectCode: string
  debObjectId: number | undefined
  debObjectName: string
  description: string
  orgUnitId: number | undefined
}
export interface ListVoucher {
  code: string
  id: number | undefined
  ignoreCheckExist?: boolean
}
export interface ListDetailInvTax {
  vatPercentType: number | undefined
  accountObjectCode: string
  accountObjectId: number | undefined
  accountObjectName: string
  accountObjectTaxCode: string
  description: string
  invoiceDate: Date | string | dayjs.Dayjs
  invoiceNumber: string
  purchaseProductGroup: number | undefined
  taxType: number | undefined
  valueBeforeTax: number | undefined
  vatAccountId: number | undefined
  vatAccountNumber: string
  vatAmount: number | undefined
  vatPercent: number | undefined
  withInvoice: true
}
export interface FilterGlVoucher {
  finalisationEachAdvance?: boolean | string | null
  fromDate?: Array<string>
  isRecord?: boolean | string | null
  page?: number | null
  toDate?: Array<string>
  size?: number | null
}
