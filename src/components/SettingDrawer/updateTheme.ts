import { replaceStyleVariables } from 'vite-plugin-theme/es/client'
import { getThemeColors, generateColors } from '@/utils/themeUtil'
import { mixLighten, mixDarken, tinycolor } from 'vite-plugin-theme/es/colorUtils'

export function updateTheme(color: string): Promise<void> {
  const colors = generateColors({
    mixDarken,
    mixLighten,
    tinycolor,
    color
  })
  return replaceStyleVariables({
    colorVariables: [...getThemeColors(color), ...colors]
  })
}
