import Table from '@/components/ASME/Table/Table.vue'
import DatePicker from '@/components/ASME/DatePicker.vue'
import Input from '@/components/ASME/Input.vue'
import HttcloudInputNumber from '@/components/ASME/InputNumber.vue'
import HttcloudInputNumberInt from '@/components/ASME/InputNumberInt.vue'
import HttcloudDrawerForm from '@/components/ASME/DrawerForm.vue'
import Breadcrumb from '@/components/ASME/Breadcrumb.vue'
import HttcloudConfirm from '@/components/ASME/Confirm.vue'
import Select from '@/components/ASME/Select.vue'
import SelectTable from '@/components/ASME/SelectTable.vue'
import MultiSelectTable from '@/components/ASME/MultiSelectTable.vue'
import BackTop from '@/components/ASME/BackTop.vue'
import { PageHeader, Row, Col } from 'ant-design-vue'
import SvgIcon from '@/components/SvgIcon/index.vue'
import mixin from '@/utils/mixins/mixins'
import { Modal, message, notification, Spin, ConfigProvider } from 'ant-design-vue'
import Radio from '@/components/ASME/Radio.vue'
import Tabs from '@/components/ASME/Tabs.vue'
import CurrencyInput from '@/components/ASME/CurrencyInput.vue'
import { Component } from '@/router/types'
import { h } from 'vue'
import Loading from '@/components/Loading/Loading.vue'
Spin.setDefaultIndicator({
  indicator: h(Loading, {
    style: {
      fontSize: '60px'
    },
    delay: 300
  })
})
ConfigProvider.config({
  theme: {
    primaryColor: 'red'
  }
})
export default {
  install(Vue: Component): void {
    // ant design componet
    Vue.component('ARow', Row)
    Vue.component('ACol', Col)
    // vsme component
    Vue.component('HttcloudTable', Table)
    Vue.component('HttcloudInput', Input)
    Vue.component('HttcloudInputNumber', HttcloudInputNumber)
    Vue.component('HttcloudInputNumberInt', HttcloudInputNumberInt)
    Vue.component('HttcloudConfirm', HttcloudConfirm)
    Vue.component('HttcloudSelect', Select)
    Vue.component('HttcloudSelectTable', SelectTable)
    Vue.component('HttcloudMultiSelectTable', MultiSelectTable)
    Vue.component('HttcloudRadio', Radio)
    Vue.component('HttcloudDatePicker', DatePicker)
    Vue.component('HttcloudPageHeader', PageHeader)
    Vue.component('HttcloudSvgIcon', SvgIcon)
    Vue.component('HttcloudBreadCrumb', Breadcrumb)
    Vue.component('HttcloudBackTop', BackTop)
    Vue.component('HttcloudTabs', Tabs)
    Vue.component('HttcloudCurrencyInput', CurrencyInput)
    Vue.component('HttcloudDrawerForm', HttcloudDrawerForm)

    Vue.config.globalProperties.$message = message
    Vue.config.globalProperties.$confirm = Modal.confirm
    Vue.config.globalProperties.$notification = notification
    Vue.config.globalProperties.$info = Modal.info
    Vue.config.globalProperties.$success = Modal.success
    Vue.config.globalProperties.$error = Modal.error
    Vue.config.globalProperties.$warning = Modal.warning
    // su dung mixin
    Vue.mixin(mixin)
  }
}
