import Model from '#/Model'

export default interface InventoryTrans extends Model {
  id: number
  orgUnitId: number
  ivTransType: number
  isTransfer: number
  refTransId: number
  ivCategoryCode: string
  ivCode: string
  ivCodePrefix?: string
  ivCodeSuffix?: string
  ivCodeNumberDigit: number
  ivMemo?: string
  accountObjectType?: string
  accountObjectCode?: string
  accountObjectName?: string
  accountObjectContactName?: string
  accountObjectContactAddress?: string
  postedDate: string
  voucherDate: string
  voucherType?: string
  formNo?: string
  signNo?: string
  quantityOriginalVoucher?: string
  isRecord: number
  recordDate?: string
  transferOrderCode?: string
  transferOrderDate?: string
  transferOrderBelongTo?: string
  contractCode?: string
  shippingMothed?: number
  transferTo?: number
  srcInventoryId?: number
  desInventoryId?: number
  totalAmount: number
  salesVoucherStatus?: number
  status?: number
}
