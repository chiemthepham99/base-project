export const IMAGE_BASE64_PREFIX = /^data:image\/[a-z]+;base64,/
export const RULES_NUMBER = {
  message: 'Vui lòng nhập đúng định dạng số',
  pattern: /^([0-9]+)(\\.([0-9]+)?)?$/
}
export const RULES_NUMBER_NO_MESSAGE = {
  message: '',
  pattern: /^([0-9]+)(\\.([0-9]+)?)?$/
}
export const RULES_REQUIRED = {
  required: true,
  message: 'Trường bắt buộc'
}
export const RULES_REQUIRED_NO_MESSAGE = {
  required: true,
  message: ''
}
export const RULES_1_TO_3 = {
  min: 1,
  max: 3,
  message: 'Chiều dài từ 1 đến 3 ký tự',
  trigger: 'change'
}
export const RULES_1_TO_10 = {
  min: 1,
  max: 10,
  message: 'Chiều dài từ 1 đến 10 ký tự',
  trigger: 'change'
}
export const RULES_1_TO_11 = {
  min: 1,
  max: 11,
  message: 'Chiều dài từ 1 đến 11 ký tự',
  trigger: 'change'
}
export const RULES_MAX_NUMBER_14 = {
  max: 14,
  message: 'Không được vượt quá giá trị cho phép',
  trigger: 'change'
}
export const RULES_10_TO_14 = {
  min: 10,
  max: 14,
  message: 'Nhập mã số thuế 10 hoặc 13 số',
  trigger: 'change'
}
export const RULES_1_TO_20 = {
  min: 1,
  max: 20,
  message: 'Chiều dài từ 1 đến 20 ký tự',
  trigger: 'change'
}
export const RULES_1_TO_50 = {
  min: 1,
  max: 50,
  message: 'Chiều dài từ 1 đến 50 ký tự',
  trigger: 'change'
}
export const RULES_1_TO_100 = {
  min: 1,
  max: 100,
  message: 'Chiều dài từ 1 đến 100 ký tự',
  trigger: 'change'
}
export const RULES_1_TO_200 = {
  min: 1,
  max: 200,
  message: 'Chiều dài từ 1 đến 200 ký tự',
  trigger: 'change'
}
export const RULES_1_TO_255 = {
  min: 1,
  max: 255,
  message: 'Chiều dài từ 1 đến 255 ký tự',
  trigger: 'change'
}
export const RULES_1_TO_400 = {
  min: 1,
  max: 400,
  message: 'Chiều dài từ 1 đến 400 ký tự',
  trigger: 'change'
}
export const RULES_1_TO_500 = {
  min: 1,
  max: 500,
  message: 'Chiều dài từ 1 đến 500 ký tự',
  trigger: 'change'
}
export const RULES_UNSIGNED = {
  pattern: /^[a-zA-Z0-9_.-\s]*$/g,
  message: 'Vui lòng nhập không dấu'
}
export const RULES_PHONE_NUMBER = {
  pattern: /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/,
  max: 11,
  message: 'Vui lòng nhập đúng định dạng số điện thoại'
}
export const RULES_ID_NO = {
  min: 9,
  max: 13,
  message: 'Nhập CCCD 9 hoặc 13 số',
  trigger: 'change'
}
export const RULES_EMAIL = {
  pattern:
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  message: 'Địa chỉ email không hợp lệ'
}
export const RULES_PASSWORD = {
  pattern: /(?=.*[A-Z]).(?=.*[!@#\$%\^&\*]).(?=.*\d).(?=.{5,})/g,
  message:
    'Mật khẩu phải độ dài tối thiểu 8 ký tự và chứa ít nhất 1 ký tự viết hoa, 1 ký tự số, 1 ký tự đặc biệt'
}
export const RULES_CHECK_CODE = {
  pattern: /^[a-zA-Z0-9_]+$/g,
  message: 'Chỉ được phép nhập ký tự chữ, số và _'
}
export const RULES_NUMBER_INT = {
  pattern: /^[0-9]*$/,
  message: 'Chỉ được phép nhập số'
}
export const RULES_NUMBER_DOUBLE = {
  pattern: /^[0-9]+\.?[0-9]*$/,
  message: 'Nhập đúng định dạng số'
}
export const RULES_TAX_CODE = {
  pattern: /^[0-9]{10}(-[0-9]{3})?$/g,
  message: 'Nhập đúng định dạng Mã số thuế'
}

/**
 Hàm dùng validate trường là phần trăm
 */
export function validatePercent(rule, value) {
  if (value) {
    const reg = new RegExp('^-?[0-9.]+$', 'g')
    if (!reg.test(value)) {
      return Promise.reject(new Error(' Chỉ cho phép nhập ký tự số'))
    } else if (Number(value) > 100) {
      return Promise.reject(new Error('Không nhập quá 100%'))
    } else {
      return Promise.resolve()
    }
  } else {
    return Promise.resolve()
  }
}
export const RULES_PERCENT = {
  validator: validatePercent,
  trigger: 'change'
}
export const RULES_NUMBER_MIN = {
  type: 'number',
  min: 1,
  message: 'Số tiền không được bỏ trống',
  pattern: /^([0-9]+)(\\.([0-9]+)?)?$/,
  trigger: 'change'
}
export const RULES_REQUIRED_NUMBER_MIN = {
  type: 'number',
  min: 1,
  required: true,
  message: 'Số tiền không được bỏ trống',
  pattern: /^([0-9]+)(\\.([0-9]+)?)?$/,
  trigger: 'change'
}
export const RULES_ONE_IN_TWO = {
  message: 'chỉ được phép nhập dự nợ hoăc dư có',
  trigger: 'change'
}
