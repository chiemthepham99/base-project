export default [
  {
    dataIndex: 'id',
    title: 'ID',
    align: 'center',
    ellipsis: true,
    width: 120
  },
  {
    dataIndex: 'email',
    title: 'Email',
    align: 'left',
    ellipsis: true,
    width: 160
  },
  {
    dataIndex: 'phone',
    title: 'phone',
    align: 'left',
    ellipsis: true,
    width: 160
  },
  {
    dataIndex: '?',
    title: 'Quốc tịch',
    align: 'left',
    ellipsis: true,
    width: 120
  },
  {
    dataIndex: 'revenue',
    title: 'Doanh thu',
    align: 'right',
    width: 160
  },
  {
    dataIndex: 'billing',
    title: 'Billing',
    align: 'right',
    width: 160
  },
  {
    dataIndex: 'verified',
    title: 'Xác thực',
    align: 'center',
    width: 160
  },
  {
    dataIndex: 'status',
    title: 'Hoạt động',
    align: 'center',
    width: 160
  },
  {
    dataIndex: 'action',
    title: 'Hành động',
    align: 'center',
    ellipsis: true,
    width: 100
  }
]
