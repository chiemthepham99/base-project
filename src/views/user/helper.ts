import { message, notification } from 'ant-design-vue'
import { getSmsCaptcha } from './service'
export const requestFailed = (err) => {
  notification['error']({
    message: 'Lỗi',
    description:
      ((err.response || {}).data || {}).message ||
      'Đã xảy ra lỗi với yêu cầu, vui lòng thử lại sau',
    duration: 4
  })
}

export const useGetCaptcha = (e, validate, state, form, registerBtn) => {
  e.preventDefault()
  validate(['mobile']).then(() => {
    state.smsSendBtn = true
    const interval = window.setInterval(() => {
      if (state.time-- <= 0) {
        state.time = 60
        state.smsSendBtn = false
        window.clearInterval(interval)
      }
    }, 1000)

    message.loading('Mã xác minh đang được gửi..', 1)

    getSmsCaptcha({ mobile: form.mobile })
      .then((res) => {
        notification['success']({
          message: 'Giới thiệu',
          description:
            '\n' + 'Mã xác minh được lấy thành công, mã xác minh của bạn là：' + res.result.captcha,
          duration: 8
        })
      })
      .catch((err) => {
        clearInterval(interval)
        state.time = 60
        state.smsSendBtn = false
        requestFailed(err)
        registerBtn && (registerBtn.value = false)
      })
  })
}
