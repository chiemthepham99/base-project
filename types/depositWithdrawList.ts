export interface DepositWithdrawList {
  id: number // ID Giao dịch thu/chi tiền gửi
  accountObjectName: string // Tên đối tượng
  accountTransMemo: string // Diễn giải
  atCode: string // Mã giao dịch
  atTypeName: string // Tên loại chứng từ (Phiếu thu/Phiếu chi)
  bankAccountNumber: string // Số tài khoản ngân hàng
  orgUnitId: number // ID chi nhánh/đơn vị/tổ chức
  orgUnitName: string // Tên chi nhánh/đơn vị/tổ chức
  postedDate: string // Ngày hạch toán
  voucherDate: string // Ngày chứng từ
  totalAmount: number // Tổng tiền
}

export interface DepositWithdraw {
  id: number // Id giao dịch thu chi
  accountObjectId: number | undefined // ID Đối tượng
  accountObjectCode: string // Mã đối tượng
  accountObjectName: string // Tên đối tượng
  accountObjectContactName: string // Người nộp
  accountObjectContactAddress: string //Địa chỉ người nộp
  accountTransCategoryId: number // Loại nghiệp vụ thu
  accountTransMemo: string // Lý do nộp
  aoEmployeeId: string // Nhân viên
  atCode: string // Số chứng từ
  atType: string // Loại giao dịch thu chi: 1: Giao dịch thu | 2: Giao dịch chi
  currencyExcRate: string // Tỷ giá
  currencyId: number // Loại tiền
  isRecord: boolean
  srcBankAccountId: string // Tài khoản nguồn
  srcBankName: string // Tên ngân hàng nguồn
  srcBankAccountNumber: string // Số tài khoản ngân hàng nguồn
  desBankAccountId: string // Tài khoản đích
  desBankName: number | string | null // Tên ngân hàng đích
  desBankAccountNumber: string // Số tài khoản ngân hàng đích
  listDetail: Array<DetailDepositWithdraw> //	Chi tiết giao dịch thu chi/Hạch toán
  listVoucherRef: Array<any> // Danh sách chứng từ tham chiếu
  listVoucherId?: Array<any>
  postedDate: any // Ngày hạch toán
  voucherDate: any // Ngày chứng từ/Ngày phiếu thu
  recordDate?: string
  totalAmount: number // Tổng tiền sau quy đổi
  totalAmountOc: number // Tổng tiền theo đơn vị tiền tệ gốc
  atCodeNumberDigit: number // Số lượng chữ số trong phiếu thu
  atCodePrefix: string // Tiền tố mã phiếu thu
  atCodeSuffix: string // Hậu tố mã phiếu thu
  isCustomer: boolean // Là khách hàng
  isEmployee: boolean // Là nhân viên
  isVendor: boolean // Là Nhà cung cấp
  orgUnitId: number // Là Nhà cung cấp
}

export interface DetailDepositWithdraw {
  accountItemId: number // Id khoản mục thu chi
  accountObjectCode: string // Mã đối tượng
  accountObjectContactAddress: string // Địa chỉ người nộp
  accountObjectContactName: string // Người nộp
  accountObjectId: number // ID Đối tượng
  accountObjectName: string // Tên đối tượng
  accountObjectType: number // Loại Đối tượng
  amount: number // Số tiền sau quy đổi tiền tệ
  amountOc: number // Số tiền theo đơn vị tiền tệ gốc
  creAccountId: number // Id tài khoản có
  creAccountNumber: string // Mã tài khoản có
  debAccountId: number // Id tài khoản nợ
  debAccountNumber: string // Mã tài khoản nợ
  description: string // Diễn giải chi tiết hạch toán thu chi
  discountAccountId: number // Id tài khoản chiết khấu
  discountAccountNumber: string // Số tài khoản chiết khấu
  discountRate: number // Tỷ lệ chiết khấu
  id: number // Id chi tiết giao dịch thu
  invoiceCode: string // Mã hóa đơn
  paidAmount: number // Số tiền thanh toán: Số thu
  paymentDueDate: string // Hạn thanh toán
  paymentTermCode: string // Mã điều khoản thanh toán
  paymentTermId: number //  Điều khoản thanh toán
  receivableAccountId: number // ID Tài khoản phải thu
  receivableAccountNumber: string // Số tài khoản phải thu
  remainingAmount: number // Số tiền còn lại: Chưa thu
  totalAmount: number // Tổng tiền: Số phải thu/Số phải chi
  totalAmountOc?: number | undefined // Là Nhà cung cấp
  voucherDescription: string // Diễn giải của chứng từ
  isEdit?: boolean
  settlementAdvanceAmount: number | null
  remainingAdvanceAmount: number | null
}
