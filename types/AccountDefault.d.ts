import Model from '#/Model'

export default interface AccountDefault extends Model {
  id: number
  accountingVoucherTypeId: number
  categoryCode: string
  name: string
  status: number
}
