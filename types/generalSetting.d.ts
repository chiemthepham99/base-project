export declare interface Setting {
  text: string
  name?: string
  icon?: string
}
