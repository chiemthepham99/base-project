import Model from '#/Model'
import { Nullable } from '#/global'
import dayjs from 'dayjs'

export interface InvoiceSales extends Model {
  aoCustomerId: Nullable<number>
  aoCustomerCode: string
  aoCustomerContactAddress: string
  aoCustomerContactName: string
  aoCustomerName: string
  aoCustomerTaxCode: string
  invFormNo: string
  invIssueDate: string | number | Date | dayjs.Dayjs | null | undefined
  invNo: string
  invSignNo: string
  paymentMethod: number
  aoBankAccountId: string
  status: Nullable<number>
}
export interface inventoryTransOut extends Model {
  aoCustomerAddress: string
  aoCustomerCode: string
  aoCustomerContactAddress: string
  aoCustomerContactName: string
  aoCustomerId: 0
  aoCustomerName: string
  aoCustomerTaxCode: string
  ivCode: string
  ivCodeNumberDigit: 0
  ivCodePrefix: string
  ivCodeSuffix: string
  ivMemo: string
  postedDate: string
  quantityOriginalVoucher: 0
  salesmanId: 0
  voucherDate: string
}
