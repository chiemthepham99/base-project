import { execute } from '@/services'
import API from '@/services/auth/defineRouting'

export async function login(params?: { email: string; password: string }) {
  return execute(API.LOGIN, params, 'post')
}
export async function logout() {
  return execute(API.LOGOUT, {}, 'post')
}
