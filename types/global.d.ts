type Nullable<T> = T | null
type Function = () => void
declare type Indexable<T = any> = {
  [key: string]: T
}
export type Crypt = (key: string, iv: string, data: string) => string
export interface LangFile {
  [x: string]: string
}
export interface I18nMsg {
  [x: string]: I18nMsgVal
}
export type I18nMsgVal = I18nMsg | string
export interface Response {
  message: string
  code: any
  body: any
}
export interface BooleanResponse extends Response {
  body: boolean
}
export interface PageMeta {
  page: number
  size: number
  total_elements: number
  total_pages: number
}
export interface PagingResponseBody {
  data: any
  page_meta: PageMeta
}
export interface PagingResponse extends Response {
  body: PagingResponseBody
}
export interface User {
  authorization: [
    {
      actionSet: [string]
      parentId: string
      rsCode: string
      rsId: string
      type: number
      uri: string
    }
  ]
  birthday: string
  email: string
  fullName: string
  listRole: [Role]
  mobile: string
  staffId: number
  userId: string
  userName: string
  customerCode: string
}

export interface Permission {
  attributes: {
    additionalProp1: [string]
    additionalProp2: [string]
    additionalProp3: [string]
  }
  clientId: string
  code: string
  description: string
  id: string
  level: number
  name: string
  parentId: string
  parentName: string
  status: number
  type: string
  uri: string
}

export interface Role {
  code: string
  description: string
  id: string
  name: string
}

// Table action
export type Action = {
  key: string
  title: string
  activeClass: string
  icon: string
  hidden: boolean
}
