import { Nullable } from '#/global'
import Model from '#/Model'

export default interface LcUnit extends Model {
  id: number
  name: string
  description?: Nullable<string>
  status: number
}
