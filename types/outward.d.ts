// import { Response } from '#/global'
import dayjs from 'dayjs'
export interface InventoryOutward {
  accountObjectCode: string
  accountObjectContactAddress: string
  accountObjectContactName: string
  accountObjectId: string
  accountObjectName: string
  contractCode: string
  desInventoryId: number
  formNo: string
  isCustomer: boolean
  isEmployee: boolean
  isVendor: boolean
  ivCategoryCode: string
  ivCode: string
  ivCodeNumberDigit: number
  ivCodePrefix: string
  ivCodeSuffix: string
  ivMemo: string
  isRecord?: boolean
  id?: number | null
  postedDate: Date | string | dayjs.Dayjs
  quantityOriginalVoucher: number
  salesmanId: number
  shippingMethod: number
  signNo: string
  srcInventoryId: number
  totalAmount: number
  transferOrderBelongTo: string
  transferOrderCode: string
  transferOrderDate: Date | string | dayjs.Dayjs
  transferTo: number
  voucherDate: Date | string | dayjs.Dayjs
  listDetail: Array<detailOutWard>
  listVoucher: Array<ListVoucherOutWard>
}
export interface ListVoucherOutWard {
  code: string
  id: number
  ignoreCheckExist: true
}
export interface detailOutWard {
  accountItemCode: string
  accountItemId: number
  accountItemName: string
  amount: number
  constructionCode: string
  constructionId: number
  constructionName: string
  costObjectCode: string
  costObjectId: number
  costObjectName: string
  creAccountId: number
  creAccountNumber: string
  debAccountId: number
  debAccountNumber: string
  desInventoryId: number
  height: number
  length: number
  orgUnitId: number
  productItemCode: string
  productItemId: number
  productItemName: string
  productionCostItemCode: string
  productionCostItemId: number
  productionCostItemName: string
  quantity: number
  radius: number
  statisticTagCode: string
  statisticTagId: number
  statisticTagName: string
  unitId: number
  unitPrice: number
  unreasonableExpense: boolean
  weight: number
  width: number
  srcInventoryId: number
}
// interface InventoryOutwardResponse extends Response {
//   body: { InventoryOutward }
// }
