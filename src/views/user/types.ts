import { Response } from '#/global'

export interface Get2Step extends Response {
  result: { stepCode: number }
}

export interface Login {
  id: string
  name: string
  username: string
  password: string
  avatar: string
  status: number
  telephone: string
  lastLoginIp: string
  lastLoginTime: number
  creatorId: string
  createTime: number
  merchantCode: string
  deleted: number
  permission: string[]
  menu: Array<any>
  code?: number
  msg?: string
}
export interface LoginRequest {
  username: string
  isSecure: boolean
  password: string
  clientId: string
  otp?: string
  deviceUuid: string
  deviceType: number
  deviceOs?: string
  browserName?: string
  deviceDescription?: string
  socketId: string
}
export interface ConfirmOtpFormState {
  otp: string
  key: string
}
export interface RequestNewPasswordResponse extends Response {
  body: boolean
}
export interface SetNewPasswordResponse extends Response {
  body: boolean
}
export interface ValidateOtpResponse extends Response {
  body: boolean
}

export interface GetSmsCaptcha extends Response {
  result: { captcha: number }
}

export interface Logout extends Response {
  result: {
    clientId: string
    refreshToken: string
  }
}

export interface GeneratePassword {
  password: string
  confirmPassword: string
}

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace API {
  export type _Get2Step = () => Promise<Get2Step>
  export type _RefreshToken = (data: { clientId?: string; refreshToken?: string }) => Promise<Login>
  export type _RequestNewPassword = (data: { email: string }) => Promise<RequestNewPasswordResponse>
  export type _SetNewPassword = (data: {
    email: string
    isSecure: boolean
    newPassword: string
    otp: string
  }) => Promise<SetNewPasswordResponse>
  export type _ValidateOtp = (data: { key: string; otp: string }) => Promise<ValidateOtpResponse>

  export type _GetSmsCaptcha = (data: { mobile: string }) => Promise<GetSmsCaptcha>
  export type _Logout = (data: { clientId: string; refreshToken: string }) => Promise<Logout>
}
// Response của API login xac thuc 2 buoc
export interface LoginResponseData {
  email: string
  menuData: [
    {
      attributes: {
        additionalProp1: [string]
        additionalProp2: [string]
        additionalProp3: [string]
      }
      clientId: string
      code: string
      description: string
      id: string
      level: number
      name: string
      parentId: string
      parentName: string
      status: number
      type: string
      uri: string
    }
  ]
  name: string
  requiredEnableTwoFa: boolean
  tokenData: {
    access_token: string
    expires_in: number
    expires_time: number
    refresh_expires_in: number
    refresh_token: string
    token_type: string
  }
  twoFA: boolean
  userId: string
  userName: string
  loginOnOtherDevice: boolean
  deviceActiveInfo: string
}
export interface LoginResponse extends Response {
  body: LoginResponseData
}
export interface RefreshTokenResponse extends Response {
  body: {
    access_token: string
    expires_in: number
    expires_time: number
    refresh_expires_in: number
    refresh_token: string
    token_type: string
  }
}
