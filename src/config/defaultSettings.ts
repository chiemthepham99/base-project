export default {
  navTheme: 'dark', // theme for nav menu
  primaryColor: '#243468',
  layout: 'sidemenu', // nav menu position: `sidemenu` or `topmenu`
  contentWidth: 'Fluid', // layout of content: `Fluid` or `Fixed`, only works when layout is topmenu
  fixedHeader: true, // sticky header
  fixSiderbar: false, // sticky siderbar
  colorWeak: false,
  title: 'HTTCloud',
  // pwa: false,
  // iconfontUrl: '',
  storage: {
    namespace: 'HTTCloud_',
    isEncrypt: import.meta.env.PROD
  },
  grayMode: false,
  darkMode: false,
  dynamicBrowserTab: true,
  useAsyncRouter: false
}
