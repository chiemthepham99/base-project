import Model from '#/Model'
export default interface LcCurrency extends Model {
  id: number
  code: string
  name: string
  cashAccountId: number
  cashInBankAccountId: number
  conversion: number
  exchangeRate: number
  vnPrefix: string
  vnCurrencyName: string
  vnSeparatorName: string
  vnDecimalAmountName: string
  vnConversionRate: number
  vnSuffix: string
  enPrefix: string
  enCurrencyName: string
  enSeparatorName: string
  enDecimalAmountName: string
  enConversionRate: number
  enSuffix: string
  status: number
}
