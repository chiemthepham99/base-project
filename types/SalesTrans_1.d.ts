import dayjs from 'dayjs'
import { Nullable } from '#/global'
export interface FilterRef {
  isRecord: boolean
  stCode: string
  postedStatus: string
  accountTransType: string
  taxInvoiceStatus: string
  createInvoiceStatus: number
  paymentStatus: number
  accountTransMemo: string
  accountObjectId: string | number
  date: Array<string>
}

export interface ReceiptPaymentList {
  accountObjectCode: string
  accountObjectContactAddress: string
  accountObjectContactName: string
  accountObjectName: string
  accountTransCode: string
  accountTransMemo: string
  accountTransTypeName: string
  aoEmployeeName: string
  bookkeepingDate: string
  createByName: string
  createTime: string
  id: number
  postedDate: string
  totalAmount: number
  totalAmountAc: number
  updateByName: string
  updateTime: string
  voucherDate: string
}

export interface SaleTranDetail {
  productItemId?: Nullable<number> // Id hàng hóa
  productItemCode: string // Mã hàng hóa
  productItemName: string // Tên hàng hóa
  costAccountId?: Nullable<number> // Id tài khoản chi phí
  costAccountNumber?: string // Số tài khoản chi phí
  costUnitPrice: Nullable<number>
  costAmount: Nullable<number>
  currencyExcRate: Nullable<number>
  inventoryId?: Nullable<number> // Số tài khoản chi phí
  inventoryAccountId?: Nullable<number> // Id tài khoản kho
  inventoryAccountNumber?: string // Số tài khoản kho
  debAccountId?: Nullable<number> //  Id tài khoản công nợ
  debAccountNumber?: string // Số tài khoản công nợ
  unitId: Nullable<number> // Đơn vị tính
  quantity: Nullable<number> // Số lượng
  unitPriceOc: Nullable<number> // Số lượng
  unitPrice: Nullable<number> // Số lượng
  amountOc: Nullable<number> // Thành tiền theo đơn vị tiền tệ gốc
  amount: Nullable<number> // Thành tiền sau quy đổi
  vatPercent: Nullable<number> // Phần trăm thuế
  vatAmountOc: Nullable<number> // Tiền thuế theo đơn vị tiền tệ gốc
  vatAmount: Nullable<number> // Tiền thuế sau quy đổi
  discountAccountId?: number
  discountAccountNumber?: number
  discountRate: Nullable<number> // Tỷ lệ chiết khấu
  discountAmountOc: Nullable<number> // Tiền chiết khấu theo đơn vị tiền tệ gốc
  discountAmount: Nullable<number> // Tiền chiết khấu sau quy đổi
  vatAccountId?: Nullable<number> // Id Tài khoản thuế
  vatAccountNumber?: Nullable<number> // Số tài khoản thuế
  inventoryAmount: Nullable<number> // Giá trị nhập kho
  isEdit?: boolean
}
interface Invoice {
  aoCustomerId: Nullable<number>
  aoCustomerCode: string
  aoCustomerContactAddress: string
  aoCustomerContactName: string
  aoCustomerName: string
  aoCustomerTaxCode: string
  invFormNo: string
  invIssueDate: string | number | Date | dayjs.Dayjs | null | undefined
  invNo: string
  invSignNo: string
  paymentMethod: string
  aoBankAccountId: string
}
export interface SaleTran {
  id?: Nullable<number>
  maxDiscount: Nullable<number>
  accountTransCategoryId?: number
  createInvoiceStatus?: Nullable<number>
  stCategoryCode: number
  accountObjectCode: string // Mã đối tượng
  discountType: string // Mã đối tượng
  taxCode: string // Mã đối tượng
  description: string // Diễn giải
  accountObjectId: Nullable<number> // ID Đối tượng
  aoCustomerId?: Nullable<number> // Id khách hàng
  aoCustomerCode?: string | undefined // Mã khách hàng
  aoCustomerName?: string | undefined // Tên khách hàng
  aoCustomerAddress?: string | undefined // Địa chỉ
  aoCustomerTaxCode?: string | undefined // Mã số thuế khách hàng
  aoCustomerContactName?: string //Người liên hệ
  aoCustomerContactAddress?: string
  aoEmployeeId: number | string // Nhân viên
  stCode: string // Mã phiếu thu
  stCodeNumberDigit: number | string | null // Số lượng chữ số trong phiếu thu
  stCodePrefix: string // Tiền tố mã phiếu thu
  stCodeSuffix: string // Hậu tố mã phiếu thu
  currencyExcRate: number // Tỷ giá
  discountRate: number //
  discountAmountOc: number //
  discountAmount: number //
  vatAmount: number //
  currencyId: Nullable<number> // Loại tiền
  isRecord: boolean
  isCustomer: boolean // Là khách hàng
  isEmployee: boolean // Là nhân viên
  isVendor: boolean // Là Nhà cung cấp
  listDetail: Array<SaleTranDetail> // Là Nhà cung cấp
  listVoucher: Array<any> // Danh sách tham chiếu
  orgUnitId: number | string // Là Nhà cung cấp
  postedDate?: string | number | Date | dayjs.FormatObject | null | undefined // Ngày hạch toán
  voucherDate?: string | number | Date | dayjs.Dayjs | null | undefined // Ngày chứng từ
  totalAmount: number // Tổng tiền: Số phải thu/Số phải chi
  totalAmountOc: number // Tổng tiền theo đơn vị tiền tệ gốc
  recordDate: string | number | Date | dayjs.Dayjs | null | undefined //Ngày ghi sổ
  vatAmountOc: number // Tiền thuế theo đơn vị tiền tệ gốc
  accountObjectContactName: string
  accountObjectContactAddress: string
  paymentTermId?: number | string | null
  paymentDueTime?: Nullable<number>
  paymentDueDate?: string | number | Date | dayjs.Dayjs | null | undefined
  dueDate: Nullable<string>
  paymentStatus: string
  itemAmount: number
  itemAmountOc: number
  includeInventoryOut: boolean
  withInvoice: boolean
  invFormNo: string
  invSignNo: string
  invoiceCode: string
  invIssueDate: string | number | Date | dayjs.Dayjs | null | undefined
  eInvoiceId?: Nullable<number>
  eInvoiceUrl?: Nullable<string>
  qualityOriginalVoucher?: Nullable<number>
  invoice: Invoice
}

export interface Filter {
  postedStatus?: boolean | string
  stCode: string
  accountTransType: string | number
  isRecord: boolean
  createInvoiceStatus: number
  fromDate: string | number | Date | dayjs.Dayjs | null | undefined
  toDate: string | number | Date | dayjs.Dayjs | null | undefined
  paymentStatus: number
}
