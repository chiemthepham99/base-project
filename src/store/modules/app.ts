import ls from '@/utils/Storage'
import { updateDarkMode } from '@/components/SettingDrawer/settingConfig'
import {
  SITE_SETTINGS,
  SET_SIDEBAR_TYPE,
  TOGGLE_DEVICE,
  TOGGLE_FIXED_HEADER,
  TOGGLE_CONTENT_WIDTH,
  TOGGLE_COLOR,
  TOGGLE_WEAK,
  TOGGLE_MULTI_TAB,
  SET_SETTING_DRAWER,
  TOGGLE_FIXED_SIDERBAR,
  TOGGLE_FIXED_HEADER_HIDDEN,
  TOGGLE_GRAY,
  TOGGLE_THEME,
  TOGGLE_LAYOUT_MODE,
  CLOSE_SIDEBAR,
  SET_DARK_MODE
} from '@/store/mutation-types'
interface State {
  sidebar: boolean
  device: string
  theme: string
  layout: string //sidemenu topmenu
  contentWidth: string //Fixed Fluid
  fixedHeader: boolean
  fixSiderbar: boolean
  autoHideHeader: boolean
  color: string | null
  weak: boolean
  gray: boolean
  multiTab: boolean
  showSettings: boolean
  darkMode: boolean
}
const app = {
  state: {
    sidebar: true,
    device: 'desktop',
    theme: 'dark',
    layout: 'sidemenu', //sidemenu topmenu
    contentWidth: 'Fluid', //Fixed Fluid
    fixedHeader: false,
    fixSiderbar: false,
    autoHideHeader: false,
    color: null,
    weak: false,
    gray: false,
    multiTab: false,
    showSettings: false,
    darkMode: false
  },
  mutations: {
    [SET_SIDEBAR_TYPE]: (state: State, type: boolean): void => {
      state.sidebar = type
      cache({ [SET_SIDEBAR_TYPE]: type })
    },
    [CLOSE_SIDEBAR]: (state: State): void => {
      cache({ [CLOSE_SIDEBAR]: true })
      state.sidebar = false
    },
    [TOGGLE_DEVICE]: (state: State, device: string): void => {
      state.device = device
    },
    [TOGGLE_THEME]: (state: State, theme: string): void => {
      cache({ [TOGGLE_THEME]: theme })
      state.theme = theme

      if (state.darkMode) {
        setDarkMode(state, false)
      }
    },
    [SET_DARK_MODE]: (state: State, isDark: boolean): void => {
      setDarkMode(state, isDark)
      if (isDark) {
        state.theme = 'dark'
      }
    },
    [TOGGLE_LAYOUT_MODE]: (state: State, layout: string): void => {
      if (layout === 'sidemenu') {
        state.contentWidth = 'Fluid'
        cache({ [TOGGLE_CONTENT_WIDTH]: 'Fluid' })
      }
      cache({ [TOGGLE_LAYOUT_MODE]: layout })
      state.layout = layout
    },
    [TOGGLE_FIXED_HEADER]: (state: State, fixed: boolean): void => {
      cache({ [TOGGLE_FIXED_HEADER]: fixed })
      state.fixedHeader = fixed
    },
    [TOGGLE_FIXED_SIDERBAR]: (state: State, fixed: boolean): void => {
      cache({ [TOGGLE_FIXED_SIDERBAR]: fixed })
      state.fixSiderbar = fixed
    },
    [TOGGLE_FIXED_HEADER_HIDDEN]: (state: State, show: boolean): void => {
      cache({ [TOGGLE_FIXED_HEADER_HIDDEN]: show })
      state.autoHideHeader = show
    },
    [TOGGLE_CONTENT_WIDTH]: (state: State, type: string): void => {
      cache({ [TOGGLE_CONTENT_WIDTH]: type })
      state.contentWidth = type
    },
    [TOGGLE_COLOR]: (state: State, color: string): void => {
      cache({ [TOGGLE_COLOR]: color })
      state.color = color
    },
    [TOGGLE_WEAK]: (state: State, flag: boolean): void => {
      cache({ [TOGGLE_WEAK]: flag })
      state.weak = flag
    },
    [TOGGLE_GRAY]: (state: State, flag: boolean): void => {
      cache({ [TOGGLE_GRAY]: flag })
      state.gray = flag
    },
    [TOGGLE_MULTI_TAB]: (state: State, bool: boolean): void => {
      cache({ [TOGGLE_MULTI_TAB]: bool })
      state.multiTab = bool
    },
    [SET_SETTING_DRAWER]: (state: State, type: boolean): void => {
      state.showSettings = type
    }
  }
}

function cache(o) {
  ls.setObj(SITE_SETTINGS, o)
}
function setDarkMode(state, isDark) {
  cache({ [SET_DARK_MODE]: isDark })
  updateDarkMode(isDark)
  state.darkMode = isDark
}

export default app
