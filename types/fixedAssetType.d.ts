export interface FixedAssetType {
  id: number
  parentId: number
  code: string
  name: string
  assetHistoricalCostAccountId: number | string
  depreciationAccountId: number | string
  description?: string
}
