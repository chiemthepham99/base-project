import Model from '#/Model'
import { Nullable } from '#/global'

export default interface Account extends Model {
  id: number
  parentId: Nullable<number>
  accountNumber: string
  accountVnName: string
  accountEnName?: Nullable<string>
  accountCnName?: Nullable<string>
  accountKrName?: Nullable<string>
  accountType?: Nullable<number>
  accountKind?: Nullable<number>
  accountDescription?: Nullable<string>
  portableInForeignCurrency?: Nullable<number>
  detailByObject?: Nullable<number>
  detailByBankAccount?: Nullable<number>
  detailByExpenseObject?: Nullable<number>
  detailByExpenseItem?: Nullable<number>
  detailByProject?: Nullable<number>
  detailByOrgUnit?: Nullable<number>
  detailByPurchaseOrder?: Nullable<number>
  detailBySalesOrder?: Nullable<number>
  detailByPurchaseContract?: Nullable<number>
  detailBySalesContract?: Nullable<number>
  debAmount?: Nullable<number>
  creAmount?: Nullable<number>
  status: number
}
