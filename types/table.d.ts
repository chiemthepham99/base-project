interface CustomCell {
  (record: any, rowIndex: number, column): void
}
export interface Columns {
  align?: 'left' | 'right' | 'center' // mac dinh left
  colSpan?: number
  customCell?: CustomCell
  customFilterDropdown?: boolean
  dataIndex: string
  key: string
  title: string
  fixed?: boolean | string
  ellipsis?: boolean
  width?: boolean | string | number
  onFilter?: any
  onFilterDropdownVisibleChange?: any
  filterMode?: any
  filterIcon?: any
}
export interface Pagination {
  pages: number
  total: number
  current: number
  pageSize: number
  showSizeChanger: boolean
  showQuickJumper: boolean
  pageSizeOptions: Array<string>
  showTotal(): any
  reset(): any
}
