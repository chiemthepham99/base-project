export interface InwardOutwardListStatistical {
  outOfStock: number
  runningOutOfStock: number
  allAmount: number
}
export interface FilterInwardOutward {
  date?: Array<string>
  isRecord: number | null
  ivTransType: number | null
  page: number | null
  size: number | null
}
