import Model from '#/Model'

export default interface ProductItem extends Model {
  id: number | null
  type: number | null
  code: string | null
  name: string | null
  avatar?: string | null
  avatarUrl?: string | null
  unitId?: number | null
  listConversionUnit: Array<any>
  listProductGroup: Array<any>
  vatDeductionResolution43?: number | null
  warrantyPeriod?: number | null
  minOnHand?: number | null
  source?: string | null
  description?: string | null
  descriptionPurchase?: string | null
  descriptionSales?: string | null
  status: number | null
}
