import Model from '#/Model'

export default interface AoGroup extends Model {
  id: number
  parentId?: number
  code: string
  name: string
  description?: string
  status: number
}
