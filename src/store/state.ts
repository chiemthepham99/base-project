export interface State {
  client: string
  title: string
}

export const state: State = {
  title: 'Vue',
  client: 'Tasco'
}
