import Model from '#/Model'
import { Nullable } from '#/global'
import dayjs from 'dayjs'

export default interface SalesTrans extends Model {
  id?: Nullable<number>
  maxDiscount: Nullable<number>
  accountTransCategoryId?: number
  createInvoiceStatus?: Nullable<number>
  stCategoryCode: number
  accountObjectCode: string // Mã đối tượng
  discountType: string // Mã đối tượng
  taxCode: string // Mã đối tượng
  description: string // Diễn giải
  accountObjectId: Nullable<number> // ID Đối tượng
  aoCustomerId?: Nullable<number> // Id khách hàng
  aoCustomerCode?: Nullable<string> // Mã khách hàng
  aoCustomerName?: string | undefined // Tên khách hàng
  aoCustomerAddress?: string | undefined // Địa chỉ
  aoCustomerTaxCode?: string | undefined // Mã số thuế khách hàng
  aoCustomerContactName?: string //Người liên hệ
  aoCustomerContactAddress?: string
  aoEmployeeId: Nullable<number | string> // Nhân viên
  stCode: string // Mã phiếu thu
  stCodeNumberDigit: number | string | null // Số lượng chữ số trong phiếu thu
  stCodePrefix: string // Tiền tố mã phiếu thu
  stCodeSuffix: string // Hậu tố mã phiếu thu
  currencyExcRate: number // Tỷ giá
  discountRate: number //
  discountAmountOc: number //
  discountAmount: number //
  vatAmount: number //
  currencyId: Nullable<number> // Loại tiền
  isRecord: boolean
  isCustomer: boolean // Là khách hàng
  isEmployee: boolean // Là nhân viên
  isVendor: boolean // Là Nhà cung cấp
  // listDetail: Array<SalesTranDetail> // Là Nhà cung cấp
  // listVoucher: Array<any> // Danh sách tham chiếu
  orgUnitId: number | string // Là Nhà cung cấp
  postedDate?: string | number | Date | dayjs.FormatObject | null | undefined // Ngày hạch toán
  voucherDate?: string | number | Date | dayjs.Dayjs | null | undefined // Ngày chứng từ
  totalAmount: number // Tổng tiền: Số phải thu/Số phải chi
  totalAmountOc: number // Tổng tiền theo đơn vị tiền tệ gốc
  recordDate: string | number | Date | dayjs.Dayjs | null | undefined //Ngày ghi sổ
  vatAmountOc: number // Tiền thuế theo đơn vị tiền tệ gốc
  totalPayment: number
  totalPaymentOc: number
  accountObjectContactName: string
  accountObjectContactAddress: string
  paymentTermId?: number | string | null
  paymentDueTime?: Nullable<number>
  paymentDueDate?: string | number | Date | dayjs.Dayjs | null | undefined
  dueDate: Nullable<string>
  paymentStatus: string
  itemAmount: number
  itemAmountOc: number
  includeInventoryOut: boolean
  withInvoice: boolean
  invFormNo: string
  invSignNo: string
  invoiceCode: string
  invIssueDate: string | number | Date | dayjs.Dayjs | null | undefined
  eInvoiceId?: Nullable<number>
  eInvoiceUrl?: Nullable<string>
  qualityOriginalVoucher?: Nullable<number>
}

export interface SalesTransList extends SalesTrans {
  aoVendorCode: string
  aoVendorId: number
  aoVendorName: string
  createByName: string
  createInvoiceStatusName: string
  invNo: string
  paymentStatusName: string
  purchaseCost: number
  quantityOriginalVoucher: number
  stCategoryName: string
  taxInvoiceStatus: number
  taxInvoiceStatusName: string
  updateByName: string
}
