export default {
  welcome: 'Chào mừng',
  home: 'Trang chủ',
  // menu
  // table
  'table.multi-action': 'Thực hiện hàng loạt',
  'table.filter-terms': 'Điều kiện tìm kiếm',
  'table.filter': 'Lọc',
  // index
  'dashboard.analysis': 'Analysis',
  'dashboard.title': 'Tổng quan',

  // Cấp đơn
  'account.title': 'Tài khoản',

  // end menu
  'profile.title': 'Profile',
  'profile.basic': 'Basic Profile',
  'profile.advanced': 'Advanced Profile',
  'result.title': 'Result',
  'result.success': 'Success',
  'result.fail': 'Fail',
  'exception.title': 'Exception',
  'exception.not-permission': '403',
  'exception.not-find': '404',
  'exception.server-error': '500',
  'exception.trigger': 'Trigger',
  'account.center': 'Account Center',
  'account.settings': 'Account Settings',
  'account.trigger': 'Trigger Error',
  'account.logout': 'Logout'
}
