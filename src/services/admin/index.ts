import { execute } from '@/services'
import API from '@/services/defineRouting'

export async function searchAccount(params: any) {
  return execute(API.SEARCH_ACCOUNT, params, 'get')
}
