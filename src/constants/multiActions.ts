export default [
  {
    key: 'use',
    icon: 'x-circle',
    activeClass: 'asme-text-red',
    title: 'Ngừng sử dụng hàng loạt',
    hidden: true
  },
  {
    key: 'ceaseUsing',
    icon: 'check-circle',
    activeClass: 'asme-text-green',
    title: 'Sử dụng hàng loạt',
    hidden: true
  },
  {
    key: 'delete',
    icon: 'delete',
    activeClass: 'asme-text-red',
    title: 'Xóa hàng loạt',
    hidden: true
  }
]
