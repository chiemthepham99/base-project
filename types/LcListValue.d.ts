export default interface LcListValue {
  code: string
  description: string
  key: string
  value: string
}
