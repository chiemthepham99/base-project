import { Nullable } from '#/global'
import Model from '#/Model'

export default interface PaymentTerm extends Model {
  id: number
  code: string
  name: string
  dueTime: Nullable<number>
  discountTime: Nullable<number>
  discountPercent: Nullable<number>
  status: number
}
