import { Router } from 'vue-router'
import { message } from 'ant-design-vue'
import store from '@/store'
// import { LoginResponseData } from '@/views/user/types'
export const loginSuccess = async (res: any, router: Router, redirect: any) => {
  message.success({
    content: 'Đăng nhập thành công',
    duration: 3
  })
  await store.dispatch('setToken', res.data.refresh_token)
  await store.dispatch('setRefreshToken', res.data.refresh_token)
  return router.push(redirect)
}
