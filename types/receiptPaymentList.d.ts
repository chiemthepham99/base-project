import dayjs from 'dayjs'

export interface FilterPaymentList {
  atCode: string
  postedStatus: string
  accountTransType: string
  accountTransMemo: string
  accountObjectId: string | number
  date: Array<string>
  isCustomer: boolean
  isEmployee: boolean
  isVendor: boolean
}
export interface ReceiptPaymentList {
  accountObjectCode: string
  accountObjectContactAddress: string
  accountObjectContactName: string
  accountObjectName: string
  accountTransCode: string
  accountTransMemo: string
  accountTransTypeName: string
  aoEmployeeName: string
  bookkeepingDate: string
  createByName: string
  createTime: string
  id: number
  postedDate: Date | string | dayjs.Dayjs
  totalAmount: number
  updateByName: string
  updateTime: string
  voucherDate: Date | string | dayjs.Dayjs
}
export interface DetailModelReceipt {
  id: null
  accountItemId: number
  accountObjectCode: string
  accountObjectContactAddress: string
  accountObjectContactName: string
  accountObjectId: number
  accountObjectName: string
  amount: number
  amountOc: number
  creAccountId: number
  creAccountNumber: string
  debAccountId: number
  debAccountNumber: string
  description: string
  discountAccountId: number
  discountAccountNumber: string
  discountAmount: number
  discountRate: number
  invoiceCode: string
  isCustomer: boolean
  isEmployee: boolean
  isVendor: boolean
  paidAmount: number
  paymentDueDate: string
  paymentTermCode: string
  paymentTermId: number
  receivableAccountId: number
  receivableAccountNumber: string
  remainingAmount: number
  totalAmount: number
  voucherDescription: string
  settlementAdvanceAmount: number | null
  remainingAdvanceAmount: number | null
}
export interface ModelReceipt {
  id?: number
  accountObjectCode: string // Mã đối tượng
  accountObjectContactAddress: string //Địa chỉ người nộp
  accountObjectContactName: string //Người nộp
  accountObjectId: number | undefined // ID Đối tượng
  accountObjectName: string // ID Đối tượng
  accountTransCategoryId: string | number // Loại nghiệp vụ thu
  accountTransMemo: string // Lý do nộp
  aoEmployeeId: string // Nhân viên
  atCode: string // Mã phiếu thu
  atCodeNumberDigit: number | undefined // Số lượng chữ số trong phiếu thu
  atCodePrefix: string // Tiền tố mã phiếu thu
  atCodeSuffix: string // Hậu tố mã phiếu thu
  byInvoice: boolean // Thu tiền theo hóa đơn
  currencyExcRate: string | undefined // Tỷ giá
  currencyId: number | undefined // Loại tiền
  isRecord: boolean
  isCustomer: boolean // Là khách hàng
  isEmployee: boolean // Là nhân viên
  isVendor: boolean // Là Nhà cung cấp
  listDetail: Array<DetailModelReceipt> // Là Nhà cung cấp
  listVoucherId: Array<any> // Danh sách tham chiếu
  listVoucherRef: Array<any> // Danh sách tham chiếu
  orgUnitId: number | undefined //Id đơn vị
  postedDate: Date | string | dayjs.Dayjs // Ngày hạch toán
  totalAmount?: number | undefined // Tổng tiền
  totalAmountOc?: number | undefined // Tổng tiền theo đơn vị tiền tệ gốc
  voucherDate?: Date | string | dayjs.Dayjs // Ngày chứng từ/Ngày phiếu thu
  quantityOriginalVoucher: string | number // Số lượng chứng từ gốc
  recordDate?: any
}
export interface Filter {
  postedStatus?: boolean | string | undefined
  createInvoiceStatus?: boolean | string | undefined
  isRecord?: boolean
  orgUnitId?: number | string | undefined
  atCode?: string | undefined
  accountTransType?: string | undefined
  accountTransMemo?: string | undefined
  fromDate?: string | undefined
  toDate?: string | undefined
  accountObjectId?: string | undefined
  paymentStatus?: string | boolean | undefined
  isCustomer?: boolean
  isEmployee?: boolean
  isVendor?: boolean
}
