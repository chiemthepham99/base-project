import ls from '@/utils/Storage'
import { User } from '#/global'
import {
  ACCESS_TOKEN,
  PERMISSION,
  REFRESH_TOKEN,
  TOKEN_EXPIRE_TIME,
  USER_INFO
} from '@/store/mutation-types'
import { logout } from '@/views/user/service'
import { clearUserInfo } from '@/utils/util'
import { message } from 'ant-design-vue'
interface State {
  info: User
  permissions: Array<string>
  token: string
  tokenExpireTime: number
  refreshToken: string
}
const account = {
  state: {
    info: ls.get(USER_INFO),
    permissions: ls.get(PERMISSION),
    token: ls.get(ACCESS_TOKEN),
    refreshToken: ls.get(REFRESH_TOKEN),
    tokenExpireTime: ls.get(TOKEN_EXPIRE_TIME)
  },
  getters: {
    user: (state: State) => state.info,
    permissions: (state: State) => state.permissions,
    token: (state: State) => state.token,
    tokenExpireTime: (state: State) => state.tokenExpireTime,
    refreshToken: (state: State) => state.refreshToken
  },
  mutations: {
    SET_USER: (state: State, user: User): void => {
      ls.setObj(USER_INFO, user)
      state.info = user
    },
    SET_PERMISSION: (state: State, permissions: Array<string>): void => {
      ls.setObj(PERMISSION, permissions)
      state.permissions = permissions
    },
    SET_TOKEN: (state: State, token: string) => {
      ls.setObj(ACCESS_TOKEN, token)
      state.token = token
    },
    SET_REFRESH_TOKEN: (state: State, token: string) => {
      ls.setObj(REFRESH_TOKEN, token)
      state.refreshToken = token
    },
    SET_TOKEN_EXPIRE_TIME: (state: State, time: number): void => {
      ls.setObj(TOKEN_EXPIRE_TIME, time)
      state.tokenExpireTime = time
    }
  },
  actions: {
    setUser: ({ commit }, user: User): void => {
      commit('SET_USER', user)
    },
    setToken: ({ commit }, token: string): void => {
      commit('SET_TOKEN', token)
    },
    setTokenExpireTime: ({ commit }, time: number): void => {
      commit('SET_TOKEN_EXPIRE_TIME', time)
    },
    setRefreshToken: ({ commit }, token: string): void => {
      commit('SET_REFRESH_TOKEN', token)
    },
    refreshToken: async ({}) => {
      try {
        return Promise.resolve(true)
      } catch (e) {
        return Promise.reject(e)
      }
    },
    getUser: async ({ commit, state }) => {
      try {
        const user = await getUserInfo(state.token)
        if (user.authorization) {
          const permissions = ['admin'] // Fake always has admin permission
          user.authorization.forEach((p) => {
            if (!permissions.includes(p.uri)) {
              permissions.push(p.uri)
            }
          })
          commit('SET_PERMISSION', permissions)
        }
        commit('SET_USER', user)
        return Promise.resolve(user)
      } catch (e) {
        return Promise.reject(e)
      }
    },
    logout: async ({ state }) => {
      try {
        const data = {
          clientId: import.meta.env.VITE_CLIENT_ID,
          refreshToken: state.refreshToken
        }
        await logout(data)
        clearUserInfo()
        message.success('Đăng xuất thành công!')
        return Promise.resolve(true)
      } catch (e) {
        return Promise.resolve(e)
      }
    }
  }
}

export default account
