import Model from '#/Model'

export default interface AccountDefaultDetail extends Model {
  id: number
  accountDefaultId: number
  code: string
  name: string
  accountFilter: string
  defaultAccountId: number
  defaultAccountNumber: string
  status: number
}
