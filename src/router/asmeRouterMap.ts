import { BasicLayout, RouteView } from '@/layouts'
import { Router } from './types'
import Dashboard from '@/views/dashboard/Index.vue'
function getView(view: string) {
  return () => import(`../views/account/${view}.vue`)
}

export const asmeRouter: Router = {
  path: '/',
  name: 'index',
  redirect: '/dashboard/analysis',
  component: BasicLayout,
  meta: {
    title: 'menu.home'
  },
  children: [
    {
      path: '/get-started',
      name: 'get_started',
      component: Dashboard,
      hidden: true,
      meta: {
        title: 'menu.get-started.title',
        keepAlive: false,
        permission: ['admin']
      }
    },

    // Dashboard
    {
      path: '/dashboard/analysis',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        title: 'menu.dashboard.title',
        icon: 'home',
        keepAlive: false,
        permission: ['admin']
      }
    },
    // user
    {
      path: '/users',
      name: 'users',
      component: getView('Index'),
      meta: {
        title: 'menu.account.title',
        icon: 'user',
        keepAlive: false,
        permission: ['admin']
      }
    },

    // Thiết lập & Quản trị (Tất cả)
    {
      path: '/admin/settings',
      name: 'admin.settings',
      component: RouteView,
      redirect: '/admin/settings/general-setting',
      hidden: true,
      hideChildrenInMenu: true,
      meta: {
        title: 'menu.admin.title',
        keepAlive: false,
        permission: ['admin']
      }
    }
  ]
}
export const systemRouter: Router = {
  path: '/system',
  name: 'system',
  redirect: '/system/option',
  component: BasicLayout,
  children: []
}
