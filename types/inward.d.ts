import dayjs from 'dayjs'
import { Response } from '#/global'

export interface InventoryInward {
  accountObjectCode: string
  accountObjectContactAddress: string
  accountObjectContactName: string
  accountObjectId?: number
  accountObjectName: string
  isCustomer: boolean
  isEmployee: boolean
  isVendor: boolean
  ivCategoryCode: string
  ivCode: string
  ivCodeNumberDigit: number
  ivCodePrefix: string
  ivCodeSuffix: string
  ivMemo: string
  isRecord?: boolean
  id?: number | null
  postedDate: Date | string | dayjs.Dayjs
  quantityOriginalVoucher: number
  totalAmount: number
  voucherDate: Date | string | dayjs.Dayjs
  listDetail: Array<detailInward>
  listVoucher: Array<ListVoucherInward>
}
export interface ListVoucherInward {
  code: string
  id: number
  ignoreCheckExist: boolean
}
export interface detailInward {
  accountItemCode: string
  accountItemId?: number | string
  accountItemName: string
  amount: number
  constructionCode: string
  constructionId?: number | string
  constructionName: string
  costObjectCode: string
  costObjectId?: number | string
  costObjectName: string
  creAccountId?: number | string
  creAccountNumber: string
  debAccountId?: number | string
  debAccountNumber: string
  desInventoryId?: number | string
  height: number
  length: number
  orgUnitId?: number | string
  productCostItemCode: string
  productCostItemId?: number | string
  productCostItemName: string
  productItemCode: string
  productItemId?: number | string
  productItemName: string
  quantity: number
  radius: number
  statisticTagCode: string
  statisticTagId?: number | string
  statisticTagName: string
  unitId?: number | string
  unitPrice: number
  unreasonableExpense: boolean
  weight: number
  width: number
}
interface InventoryInwardResponse extends Response {
  body: InventoryInward
}
