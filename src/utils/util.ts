import ls from '@/utils/Storage'
import dayjs from 'dayjs'
import { PageMeta } from '#/global'
import { Pagination } from '#/table'
import { AesEncryption } from '@/utils/encrypt'
import { iv, key } from '@/constants/encrypt'
import { cloneDeep } from 'lodash'

export function clearUserInfo(): void {
  const deviceUuid = ls.get('SOCK_UUID')
  ls.clear()
  if (deviceUuid) {
    ls.set('SOCK_UUID', deviceUuid)
  }
}

export function timeFix(): number {
  const time = new Date()
  return time.getHours()
}

export const encryptKeys = {
  key: 'asme@20222022202',
  iv: 'CZGdXz0NGkXtV0bI'
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const getQueryParameters = (options: any) => {
  const url = options.url
  const search = url.split('?')[1]
  if (!search) {
    return {}
  }
  return JSON.parse(
    '{"' +
      decodeURIComponent(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') +
      '"}'
  )
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const getBody = (options) => {
  return options.body && JSON.parse(options.body)
}

export function scorePassword(pass: string): number {
  let score = 0
  if (!pass) {
    return score
  }
  // award every unique letter until 5 repetitions
  const letters = {}
  for (let i = 0; i < pass.length; i++) {
    letters[pass[i]] = (letters[pass[i]] || 0) + 1
    score += 5.0 / letters[pass[i]]
  }

  // bonus points for mixing it up
  const variations = {
    digits: /\d/.test(pass),
    lower: /[a-z]/.test(pass),
    upper: /[A-Z]/.test(pass),
    nonWords: /\W/.test(pass)
  }

  let variationCount = 0
  for (const check in variations) {
    variationCount += variations[check] === true ? 1 : 0
  }
  score += (variationCount - 1) * 10

  return score
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const firstLetterIsUpperCase = function (str: string) {
  const reg = /^[A-Z][A-z0-9]*$/
  return reg.test(str)
}

export const separator = ';'

export const divisionStringToArray = (string: string, customSeparator: never): any => {
  return string ? string.split(customSeparator || separator) : []
}

export function handlePaginationData({ body }) {
  if (body) {
    return {
      total: (body['pageable'] && body['pageable']['offset']) || 0
    }
  }
  return {}
}

/**
 * @rule:
 * Quy tắc encode password:
 *  - Input: raw password
 *   B1: encode raw password theo base64
 *   B2: get date hiện tại theo fomat "yyyyMMddhhmmss", ví dụ: 20220622085112(giải nghĩa: 2022/06/22 08:51:12)
 *   B3: encode date vừa get được theo base64
 *   B4: cộng chuỗi giá trị ở B1 và B3
 *   B5: encode chuỗi giá trị ở B4 theo kiểu base64
 * @use: sử dụng để mã hóa mật khẩu
 * @param password
 * @return: string (mật khẩu đã mã hóa base64)
 */
export function encodePassword(password: string): string {
  const encodePassword = btoa(password) // step one
  const currentTime = btoa(dayjs().format('YYYYMMDDhhmmss'))
  return btoa(encodePassword + currentTime)
}

export function getBase64(file: File): Promise<string> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result as string)
    reader.onerror = (error) => reject(error)
  })
}

export const isDev = import.meta.env.DEV
export const baseAuthURL = import.meta.env.VITE_AUTH_API_URL
export const baseCusSysURL = import.meta.env.VITE_API_CUS_SYS_URL
export const baseCwpFamURL = import.meta.env.VITE_API_CWP_FAM_URL

export const baseProduct = import.meta.env.VITE_API_PRODUCT

/**
Hàm dùng format giá trị input dang 1,000,000
*/
export function formatPriceNotZero(value: number): number | string {
  if (value) {
    const val = value.toFixed(0).replace(',', '.')
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  }
  return '0'
}

/**
 Hàm dùng trimspace giá trị trong ô input
 */
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function trimSpace(obj: any) {
  for (const [k, v] of Object.entries(obj)) {
    if (typeof v === 'string') {
      obj[k] = v.trim()
    } else if (typeof v === 'object') {
    }
  }
}
/**
 * @use: custom params
 * @param filter
 * @param pagination
 * @return: object
 */

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function customParam(filter: any, pagination: Pagination): any {
  return {
    ...filter,
    ...{
      page: pagination.current - 1,
      size: pagination.pageSize
    }
  }
}
export function getPagination(pagination: Pagination, pageMeta: PageMeta): any {
  return {
    ...pagination,
    ...{ total: pageMeta.total_elements || 0 }
  }
}
/**
 * @use: removeDom
 * @return: void
 * @param id
 * @param timeout
 */
export function removeLoadingAnimate(id = '', timeout = 1500): void {
  if (id === '') {
    return
  }
  const elementId = document.getElementById(id)
  if (elementId) {
    setTimeout(() => {
      document.body.removeChild(elementId)
    }, timeout)
  }
  return
}
/**
 * @use: flatArr
 * @return: Array
 * @param arr
 * @param key
 */
export default function flatArr(arr: Array<any>, key = 'listChild'): Array<any> {
  const result: Array<any> = []
  arr.map((obj) => {
    if (obj[key]) {
      const el: any = { ...obj, ...{} }
      delete el[key]
      result.push(el)
      Object.values(obj[key]).map((v) => {
        result.push(v)
      })
    } else {
      result.push(obj)
    }
  })
  return result
}

/**
 * @use: deepFlatArray
 * @return: Array
 * @param arr
 * @param key
 */
export function deepFlatArray(arr: Array<any>, key = 'listChild'): Array<any> {
  const result: Array<any> = []
  recursiveDeepFlatArray(arr, key, result)
  return result
}

function recursiveDeepFlatArray(arr: Array<any>, key, result): void {
  const currentArr = cloneDeep(arr)
  currentArr.forEach((obj) => {
    if (obj[key] && Array.isArray(obj[key]) && obj[key].length) {
      const el: any = cloneDeep(obj)
      const item = { ...el, isLastChild: false }
      delete item[key]
      result.push(item)
      recursiveDeepFlatArray(el[key], key, result)
      delete el[key]
    } else {
      delete obj[key]
      result.push({ ...obj, isLastChild: true })
    }
  })
  return
}

export const encrypt = new AesEncryption({
  key: key,
  iv: iv
})

export function objectMapping(fromObj, toObj) {
  Object.keys(toObj).forEach((key) => {
    if (fromObj.hasOwnProperty(key)) {
      toObj[key] = fromObj[key]
    }
  })
  return toObj
}
export function removeDuplicates(obj: object) {
  return obj.filter(
    (thing, index, self) =>
      index === self.findIndex((t) => JSON.stringify(t) === JSON.stringify(thing))
  )
}
export function scrollToFirstError() {
  const element = document.getElementsByClassName('ant-form-item-has-error')
  if (!!element && element[0]) {
    element[0].scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'nearest' })
  }
}
export const hashString = Math.floor(Math.random() * 90000) + 10000
