import { state } from './state'
import app from './modules/app'
import account from './modules/account'
import { createStore } from 'vuex'

const store = createStore({
  state,
  mutations: {},
  actions: {},
  modules: {
    app,
    account
  }
})

export default store
