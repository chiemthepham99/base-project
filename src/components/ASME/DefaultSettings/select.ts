// export function removeUtf8(val: string) {
//   let str = String(val)
//   str = str.toLowerCase()
//   str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
//   str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
//   str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
//   str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
//   str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
//   str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
//   str = str.replace(/đ/g, 'd')
//   str = str.replace(/[!@%^*()+=<>?/,.:;'"&#[\]~$_`\-{}|\\]/g, ' ')
//   str = str.replace(/ + /g, ' ')
//   str = str.trim()
//   return str
// }
// // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
// function filterSelectOption(input, option) {
//   const allowArr = Object.entries(option)
//   // console.log(allowArr)
//   // return
//   const texSearch = allowArr[1][0]
//   if (texSearch) {
//     const val = removeUtf8(option[texSearch])
//     return val.toLowerCase().indexOf(input.toLowerCase()) >= 0
//   }
//   return
// }
export default {
  allowClear: true,
  showCount: true,
  showArrow: true,
  placeholder: 'Nhấn để chọn',
  class: 'w-full',
  showSearch: true,
  filterOption: false,
  fieldNames: {
    label: 'name',
    value: 'id',
    option: 'option'
  }
}
