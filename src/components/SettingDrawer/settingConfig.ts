const colorList = [
  {
    key: 'yellow',
    color: '#FAAD14'
  },
  {
    key: 'primary',
    color: '#243468'
  },
  {
    key: 'red',
    color: '#F5222D'
  },
  {
    key: 'orange',
    color: '#FA541C'
  },
  {
    key: 'cyan',
    color: '#13C2C2'
  },
  {
    key: 'success',
    color: '#52C41A'
  },
  {
    key: 'dark_blu',
    color: '#2F54EB'
  },
  {
    key: 'violet',
    color: '#722ED1'
  }
]
const updateDarkMode = (isDark: boolean): void => {
  let styleTag = document.getElementById('themeCss')
  const appTag = document.getElementById('html')
  if (!styleTag) {
    styleTag = document.createElement('link')
    styleTag.setAttribute('id', 'themeCss')
    document.head.appendChild(styleTag)
  }
  if (isDark) {
    styleTag.setAttribute('href', '/themes/dark.css')
    if (appTag) {
      appTag.className = 'darkMode'
    }
  } else {
    styleTag.setAttribute('href', '')
    if (appTag) {
      appTag.className = ''
    }
  }
}

const updateColorWeak = (isColorWeak: boolean): void => {
  const app = document.getElementById('html')
  isColorWeak ? app && app.classList.add('colorWeak') : app && app.classList.remove('colorWeak')
}

const updateGrayMode = (isGrayMode: boolean): void => {
  const app = document.getElementById('html')
  isGrayMode ? app && app.classList.add('grayMode') : app && app.classList.remove('grayMode')
}

export { colorList, updateColorWeak, updateGrayMode, updateDarkMode }
