import dayjs from 'dayjs'

export interface Invoice {
  aoVendorCode: string | null // Mã nhà cung cấp
  aoVendorContactAddress: string | null // Địa chỉ giao hàng
  aoVendorContactName: string | null // Tên người giao hàng
  aoVendorId: number | null // Id nhà cung cấp
  aoVendorName: string | null // Tên nhà cung cấp
  aoVendorTaxCode: string | null // Mã số thuế
  invFormNo: string | null // Mẫu số hóa đơn
  invIssueDate: Date | string | dayjs.Dayjs | null // Ngày hóa đơn. Định dạng yyyy-MM-dd
  invNo: string | null // Số hóa đơn
  invSignNo: string | null // Ký hiệu hóa đơn
}
export interface Voucher {
  code: string | null
  id: number | null
}
export interface ListDetailPurchaseTransaction {
  amount: number | null // Thành tiền sau quy đổi
  amountOc: number | null // Thành tiền theo đơn vị tiền tệ gốc
  currencyExcRate?: number | null // Tỷ giá tiền tệ
  costAccountId?: number | null // Id tài khoản chi phí
  costAccountNumber?: number | null // Số tài khoản chi phí
  debAccountId: number | null // Id tài khoản công nợ
  debAccountNumber: string // Số tài khoản công nợ
  discountAmount: number | null // Tiền chiết khấu sau quy đổi
  discountAmountOc: number | null // Tiền chiết khấu theo đơn vị tiền tệ gốc
  discountRate: number | null // Tỷ lệ chiết khấu
  discountType?: number | null
  height: number | null // Chiều cao
  id: number | null // Id chi tiết giao dịch mua hàng
  inventoryAccountId: number | null // Id tài khoản kho
  inventoryAccountNumber: string | null // Số tài khoản kho
  inventoryAmount: number | null // Giá trị nhập kho
  inventoryId: number | null // Id kho
  isEdit?: boolean
  length: number | null // Chiều dài
  productItemCode: string // Mã hàng hóa
  productItemId: number | null // Id hàng hóa
  productItemName: string // Tên hàng hóa
  purchaseCost: number | null // Chi phí mua hàng
  purchaseOrderId: number | null
  purchaseProductGroup: number | string | null // Nhóm hàng hóa mua vào
  quantity: number | null // Số lượng
  radius: number | null // Đường kính
  status?: number | null
  unitId: number | null // Đơn vị tính
  unitPrice: number | null // Đơn giá sau quy đổi
  unitPriceOc: number | null // Đơn giá theo đơn vị tiền tệ gốc
  vatAccountId: number | null // Id Tài khoản thuế
  vatAccountNumber: string // Số tài khoản thuế
  vatAmount: number | null // Tiền thuế sau quy đổi
  vatAmountOc: number | null // Tiền thuế theo đơn vị tiền tệ gốc
  vatPercent: number | null // Phần trăm thuế
  vatPercentType: string | null // Phần trăm thuế
  weight: number | null // Khối lượng
  width: number | null // Chiều rộng
  totalAmount: number
}
export interface ListVoucherPurchaseTransaction {
  code: string // Mã chứng từ
  id: number | null // ID chứng từ
}
export interface PurchaseTransaction {
  maxDiscount?: number
  accountObjectId?: number | null // Id đối tượng hạch toán
  aoEmployeeId: string | null // Id nhân viên mua hàng
  aoVendorCode: string | null // Mã nhà cung cấp
  aoVendorContactAddress: string | null // Địa chỉ giao hàng
  aoVendorContactName: string | null // Tên người giao hàng
  aoVendorId: number | null // Id nhà cung cấp
  aoVendorName: string | null // Tên nhà cung cấp
  aoVendorTaxCode: string | null // Mã số thuế
  currencyExcRate?: number | null // Tỷ giá tiền tệ
  currencyId?: number | null // Id loại tiền
  description: string | null // Diễn giải
  discountAmount: number | null // Tiền chiết khấu
  discountAmountOc: number | null // Tiền chiết khấu theo đơn vị tiền tệ gốc
  discountRate: number | null // Tỷ lệ chiết khấu
  discountType: number | string | null // Loại chiết khấu
  einvoiceId: string | null
  einvoiceUrl: string | null
  id?: number | null // Id giao dịch mua hàng
  inventoryAmount: number | null // Giá trị nhập kho
  inventoryAmountOc?: number | null // Giá trị nhập kho theo đơn vị tiền tệ gốc
  invoice: Invoice | null
  isPurchaseCost: boolean // Là chi phí mua hàng
  isRecord?: boolean // Trạng thái ghi sổ
  itemAmount: number | null // Tổng tiền hàng
  itemAmountOc?: number | null // Tổng tiền hàng theo đơn vị tiền tệ gốc
  listDetail: Array<ListDetailPurchaseTransaction>
  listVoucher: Array<ListVoucherPurchaseTransaction>
  paymentDate?: Date | string | dayjs.Dayjs // Ngày hoàn thành thanh toán
  paymentDueDate?: string | null // Hạn thanh toán
  paymentDueTime?: string | null // Số ngày được nợ
  paymentStatus: number | null // Trạng thái thanh toán
  paymentTermId?: number | null // Điều khoản thanh toán
  postedDate: Date | string | dayjs.Dayjs // Ngày hạch toán
  ptCategoryCode: number | string | null // Mã phân loại nghiệp vụ mua hàng | Mã chứng từ
  ptCode: string | null // Số chứng từ
  ptCodeNumberDigit?: number | string // Số lượng chữ số trong mã giao dịch mua hàng
  ptCodePrefix?: string //
  ptCodeSuffix?: string //
  purchaseCost: number | null // Chi phí mua hàng
  quantityOriginalVoucher: number | null // Số chứng từ gốc đi kèm
  recordDate?: string | null // Ngày ghi sổ
  remainingAmount?: number | null // Số tiền còn lại chưa thanh toán
  status?: number | null // Trạng thái giao dịch mua hàng
  taxInvoiceStatus: number | string | null // Trạng thái nhận hóa đơn
  taxInvoiceStatusName?: string // Tên trạng thái nhận hóa đơn
  totalAmount: number | null // Tổng tiền sau quy đổi
  totalAmountOc?: number | null // Tổng tiền theo đơn vị tiền tệ gốc
  totalPayment?: number | null // Tổng tiền thanh toán
  totalPaymentOc?: number | null
  vatAmount: number | null // Tiền thuế sau quy đổi
  vatAmountOc?: number | null // Tiền thuế theo đơn vị tiền tệ gốc
  voucherDate: Date | string | dayjs.Dayjs // Ngày chứng từ
}
export interface FilterPurchase {
  aoEmployeeId?: number | null
  date?: Array<string>
  isRecord?: boolean | string | null
  page?: number | null
  paymentStatus?: number | null
  size?: number | null
  taxInvoiceStatus?: number | null
  aoVendorId?: number | null
}
