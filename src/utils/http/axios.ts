import axios, { AxiosResponse } from 'axios'
import { message } from 'ant-design-vue'
import { ACCESS_TOKEN, USER_INFO } from '@/store/mutation-types'
import { baseAuthURL } from '@/utils/util'
import ls from '@/utils/Storage'
// import store from '@/store'
import { useRouter } from 'vue-router'
import { globalLoading } from '@/store/reactiveState'

const ContentType = {
  urlencoded: 'application/x-www-form-urlencoded;charset=UTF-8',
  json: 'application/json',
  formData: 'multipart/form-data'
}

const axiosConfigs = (config) => {
  globalLoading.value = true
  const token = ls.get(ACCESS_TOKEN)
  const userinfo = ls.get(USER_INFO)
  if (token) {
    config.headers['Authorization'] = `Bearer ${token}`
  }
  if (userinfo) {
    config.headers['username'] = userinfo.username
  }

  config.headers['Content-Type'] =
    ContentType[config.data instanceof FormData ? 'formData' : 'json']
  return config
}

const requestOnReject = (error) => {
  globalLoading.value = false
  return Promise.reject(error)
}

const axiosResponse = (res: AxiosResponse<any>) => {
  globalLoading.value = false
  if (res.status === 200) {
    return Promise.resolve(res.data)
  } else if (res.status === 401 || res.data.code === 401 || res.data.code === '401') {
    message.error('\n' + 'Đăng nhập đã hết hạn vui lòng đăng nhập lại!')
    window.location.href = '/user/re-login?redirect=' + encodeURIComponent(window.location.href)
    return false
  } else if (res.status === 403) {
    message.error('\n' + 'Bạn không đủ quyền thực hiện chức năng này!').then((r) => console.log(r))
    return false
  } else if (res.status === 500) {
    message
      .error('\n' + 'Yêu cầu dữ liệu không thành công, vui lòng thử lại!')
      .then((r) => console.log(r))
    return false
  } else if (res.status === 406) {
    message
      .error('\n' + 'Đã hết thời gian đăng nhập, vui lòng đăng nhập lại!')
      .then((r) => console.log(r))
    const router = useRouter()
    router.push({ name: 'login' }).then((r) => console.log(r))
    return false
  } else if (res.status === 400) {
    message.error(res.status).then((r) => console.log(r))
    return Promise.reject(res)
  } else {
    if (window.localStorage.getItem('lang') === 'en') {
      message.error(res.data.enMsg).then((r) => console.log(r))
    } else {
      message.error(res.data.zhMsg).then((r) => console.log(r))
    }
    return false
  }
  // return res
}

const axiosResponseError = async (error) => {
  globalLoading.value = false
  const msg = error.message
  const response = error.response
  if (!response) {
    return Promise.reject(error)
  }
  if (response.status === 500) {
    message.error('Đã có lỗi xảy ra!')
    return Promise.reject(error)
  }
  if (response) {
    const { data } = response
    message.error(data.msg || data.enMsg || data.message)
  } else if (msg) {
    if (msg === 'Network Error') {
      message.error('Lỗi mạng, vui lòng kiểm tra mạng!')
    } else {
      message.error(msg)
    }
  } else {
    message.error('\n' + 'Lỗi không xác định, vui lòng thử lại!')
  }
  return Promise.reject(error)
}
// Base CWP FAM Axios Service
// request interceptor

// Base COP SYS Axios Service
const authService = axios.create({
  baseURL: baseAuthURL,
  timeout: 60000,
  responseType: 'json',
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json'
  },
  credentials: 'same-origin'
})
authService.interceptors.request.use(axiosConfigs, requestOnReject)

authService.interceptors.response.use(axiosResponse, axiosResponseError)

// Base Master Data

export { authService }
