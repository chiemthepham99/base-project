import { UserConfig, ConfigEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import Components from 'unplugin-vue-components/vite'
import vueJsx from '@vitejs/plugin-vue-jsx'
import path from 'path'
import legacy from '@vitejs/plugin-legacy'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
import { VitePWA } from 'vite-plugin-pwa'
import eslintPlugin from 'vite-plugin-eslint'
import { AntDesignVueResolver } from 'unplugin-vue-components/resolvers'

const pathResolve = (pathStr: string) => {
  return path.resolve(__dirname, pathStr)
}

export default ({}: ConfigEnv): UserConfig => {
  return {
    server: {
      host: '0.0.0.0'
    },
    plugins: [
      eslintPlugin({
        emitWarning: true
      }),
      vue(),
      vueJsx(),
      Components({ resolvers: [AntDesignVueResolver()], directoryAsNamespace: true, dts: true }),
      legacy({
        targets: ['defaults', 'not IE 11']
      }),
      createSvgIconsPlugin({
        iconDirs: [path.resolve(process.cwd(), 'src/assets/icons')],
        symbolId: 'icon-[dir]-[name]'
      }),
      /*viteThemePlugin({
        colorVariables: [...getThemeColors()]
      }),*/
      VitePWA({
        registerType: 'autoUpdate',
        injectRegister: 'auto',
        includeAssets: ['favicon.ico'],
        devOptions: {
          enabled: false
          /* other options */
        },
        manifest: {
          name: 'Tasco insurance',
          short_name: 'Tasco insurance',
          description: 'Phần mềm bảo hiểm',
          theme_color: '#ffffff',
          icons: [
            {
              src: 'pwa-192x192.png',
              sizes: '192x192',
              type: 'image/png'
            },
            {
              src: 'pwa-512x512.png',
              sizes: '512x512',
              type: 'image/png'
            }
          ]
        }
      })
    ],
    resolve: {
      alias: [
        {
          find: 'vue-i18n',
          replacement: 'vue-i18n/dist/vue-i18n.cjs.js'
        },
        {
          find: '@',
          replacement: pathResolve('src') + '/'
        }
      ]
    },
    css: {
      preprocessorOptions: {
        less: {
          // modifyVars: generateModifyVars(),
          javascriptEnabled: true
        }
      }
    }
  }
}
