import AoGroup from './AoGroup'
import Model from '#/Model'

type ListAoGroup = AoGroup[]

export default interface AoCustomer extends Model {
  contactAddress: string
  id: number
  accountObjectId: number
  listAoGroup: ListAoGroup
  aoEmployeeId: number
  customerType: string
  taxCode: string
  customerCode: string
  prefixName: string
  customerName: string
  customerTel: string
  customerAddress: string
  customerWebsite: string
  contactPrefix: string
  contactName: string
  contactEmail: string
  contactTel: string
  contactMobile: string
  contactIdNo: string
  contactIdIssueDate: string
  contactIdIssuePlace: string
  contactLegalRepresentative: string
  contactEInvName: string
  contactEInvEmail: string
  contactEInvTel: string
  paymentTermId: number
  dueTime: number
  maxDebtAmount: number
  receiveAccountId: number
  note: string
  status: number
  createAt: string
  updateAt: string
}
