import NProgress from 'nprogress' // progress bar
import '@/components/NProgress/nprogress.less' // progress bar custom style
import { hasRoutePermission } from './permission'
import { setDocumentTitle } from '@/utils/domUtil'
import type { Router } from 'vue-router'
import store from '@/store/index'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import timezone from 'dayjs/plugin/timezone'
NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = [
  'login',
  're_login',
  'forgot_password',
  'forgot_password.confirm',
  'forgot_password.set_new_password',
  'active_account'
]

const defaultRoutePath = '/dashboard/analysis'

export const setupBeforeEach = (router: Router): void => {
  router.beforeEach(async (to, from, next) => {
    NProgress.start()
    setDocumentTitle(to)
    if (store.getters.token) {
      dayjs.extend(utc)
      dayjs.extend(timezone)
      const now = dayjs().tz('Asia/Ho_Chi_Minh').unix()
      if (!store.getters.tokenExpireTime || now > store.getters.tokenExpireTime) {
        const refresh = await store.dispatch('refreshToken')
        if (refresh !== true) {
          const rs = await store.dispatch('logout')
          if (rs === true) {
            await next({ name: 'login' })
          }
        }
      }
      if (to.path === '/user/re-login') {
        next()
      } else if (to.path === '/user/login') {
        next({ path: defaultRoutePath })
      } else {
        next()
        if (hasRoutePermission(to)) {
          next()
        } else {
          next({ path: '/exception/403' })
        }
      }
    } else {
      if (whiteList.includes(to.name as any)) {
        next()
      } else {
        next({ path: '/user/login', query: { redirect: to.fullPath } })
      }
    }
  })
}

export const setupAfterEach = (router: Router): void => {
  router.afterEach(() => {
    NProgress.done() // finish progress bar
  })
}
