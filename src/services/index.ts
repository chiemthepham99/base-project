import { authService } from '@/utils/http/axios'
import { Response } from '#/global'
import { OptionsExecute } from '#/optionsExecute'
export function execute(
  url: string,
  params?: any,
  method = 'get',
  configs: OptionsExecute = {}
): Promise<Response> {
  return ['get', 'request', 'delete', 'head', 'options'].includes(method)
    ? authService[method](url, { params: params })
    : authService[method](url, params, configs)
}
