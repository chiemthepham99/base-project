export default interface Model {
  createTime?: string
  createBy?: string
  updateTime?: string
  updateBy?: string
}
