if (location.protocol === 'https:') {
  caches.keys().then(function (names) {
    for (const name of names) caches.delete(name).then((r) => console.log(r))
  })
}
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/style/global.less'
import i18n from '@/locales/useI18n'
import 'virtual:svg-icons-register'
import plugins from '@/plugins/index'
import setupDefaultSetting from '@/utils/setupDefaultSetting'
import withUUID from 'vue-uuid'
const app = withUUID(createApp(App))
app.use(router)
app.use(store)
app.use(i18n)
app.use(plugins, {})
app.mount('#app')
setupDefaultSetting()
