declare type Indexable<T = any> = {
  [key: string]: T
}

export interface GlobalParam {
  code: string
  createDate: string
  description: string
  lcParamId: number
  name: string
  status: string
  statusName: string
  value: string
}
